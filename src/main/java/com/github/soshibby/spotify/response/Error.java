package com.github.soshibby.spotify.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Error {
	@JsonProperty("type")
	private String mType;
	
	@JsonProperty("message")
	private String mMessage;
}
