package com.github.soshibby.spotify.response;

public enum AlbumArtSize {
	SIZE_160,
    SIZE_320,
    SIZE_640
}
