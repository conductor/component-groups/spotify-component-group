package com.github.soshibby.spotify.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpotifyToken {
	@JsonProperty("t")
	private String mToken;
	
	public String getToken(){
		return mToken;
	}
}
