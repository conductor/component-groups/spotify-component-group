package com.github.soshibby.spotify.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Status {
	@JsonProperty("version")
	private String mVersion;
	
	@JsonProperty("playing")
	private boolean mIsPlaying;
	
	@JsonProperty("track")
	private Track mTrack;
	
	@JsonProperty("playing_position")
	private double mPlayingPosition;
	
	@JsonProperty("volume")
	private double mVolume;
	
	@JsonProperty("running")
	private boolean mRunning;
	
	public String getVersion(){
		return mVersion;
	}
	
	public boolean isPlaying(){
		return mIsPlaying;
	}
	
	public Track getTrack(){
		return mTrack;
	}
	
	public double getPlayingPosition(){
		return mPlayingPosition;
	}
	
	public double getVolume(){
		return mVolume;
	}
	
	public boolean isRunning(){
		return mRunning;
	}
}
