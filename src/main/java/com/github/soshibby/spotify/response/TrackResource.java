package com.github.soshibby.spotify.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackResource {
	@JsonProperty("name")
	private String mName;
	
	@JsonProperty("uri")
	private String mURI;
	
	public String getName(){
		return mName;
	}
	
	public String getURI(){
		return mURI;
	}
}
