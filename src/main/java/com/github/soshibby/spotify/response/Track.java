package com.github.soshibby.spotify.response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Track {
	@JsonProperty("length")
	private int mLength;
	
	@JsonProperty("track_type")
	private String mTrackType;
	
	@JsonProperty("track_resource")
	private TrackResource mTrackResource;
	
	@JsonProperty("artist_resource")
	private TrackResource mArtistResource;
	
	@JsonProperty("album_resource")
	private TrackResource mAlbumResource;
	
	public int getLength(){
		return mLength;
	}
	
	public String getTrackType(){
		return mTrackType;
	}
	
	public TrackResource getTrackResource(){
		return mTrackResource;
	}
	
	public TrackResource getArtistResource(){
		return mArtistResource;
	}
	
	public TrackResource getAlbumResource(){
		return mAlbumResource;
	}
	
	public String getAlbumArtURL(AlbumArtSize size) throws ClientProtocolException, IOException
    {
        if (mAlbumResource.getURI() == null || mAlbumResource.getURI().contains("local"))
            return null;

        int albumsize = 0;
        switch (size)
        {
            case SIZE_160:
                albumsize = 160;
                break;
            case SIZE_320:
                albumsize = 320;
                break;
            case SIZE_640:
                albumsize = 640;
                break;
        }

        HttpClient client = new DefaultHttpClient();
        String response = "";
		
        try{
	        String albumId = mAlbumResource.getURI().split(":")[2];
	        String url = "http://open.spotify.com/album/" + albumId;
			HttpGet request = new HttpGet(url);
			HttpResponse httpResponse = client.execute(request);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
			
			String line;
			while((line = reader.readLine()) != null){
				response += line;
			}
			
			reader.close();
        }finally{
        	client.getConnectionManager().shutdown();
        }
		
        //Get the start index of the image url
        int index = response.indexOf("og:image");
        if (index == -1) return null;
        response = response.substring(index);

        //Get the beginning of the url
        index = response.indexOf("http");
        if (index == -1) return null;
        response = response.substring(index);

        //Get the end of the url
        index = response.indexOf('\"');
        if (index == -1) return null;
        response = response.substring(0, index);

        //Add the albumsize of choice
        response = response.replace("/image/", "/" + albumsize + "/");
        return response;
    }
}
