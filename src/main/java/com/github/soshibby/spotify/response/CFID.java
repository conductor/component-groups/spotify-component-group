package com.github.soshibby.spotify.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CFID {
	@JsonProperty("token")
	private String mToken;
	
	@JsonProperty("version")
	private String mVersion;
	
	@JsonProperty("client_version")
	private String mClientVersion;
	
	@JsonProperty("running")
	private boolean mRunning;
	
	@JsonProperty("error")
	private Error mError;
	
	public String getToken(){
		return mToken;
	}
	
	public String getVersion(){
		return mVersion;
	}
	
	public String getClientVersion(){
		return mClientVersion;
	}
	
	public boolean isRunning(){
		return mRunning;
	}
	
	public Error getError(){
		return mError;
	}
}
