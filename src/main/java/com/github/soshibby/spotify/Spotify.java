package com.github.soshibby.spotify;

import com.github.soshibby.spotify.facades.SpotifyFacade;
import com.github.soshibby.spotify.response.Status;
import com.github.soshibby.spotify.utils.SpotifyUpdateThread;
import org.conductor.component.annotations.Property;
import org.conductor.component.annotations.ReadOnlyProperty;
import org.conductor.component.types.Component;
import org.conductor.component.types.DataType;
import org.conductor.database.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@org.conductor.component.annotations.Component(type = "spotify")
@ReadOnlyProperty(name = "length", dataType = DataType.INTEGER, defaultValue = "0")
@ReadOnlyProperty(name = "artistURI", dataType = DataType.STRING, defaultValue = "")
@ReadOnlyProperty(name = "albumURI", dataType = DataType.STRING, defaultValue = "")
@ReadOnlyProperty(name = "track", dataType = DataType.STRING, defaultValue = "")
@ReadOnlyProperty(name = "artist", dataType = DataType.STRING, defaultValue = "")
@ReadOnlyProperty(name = "album", dataType = DataType.STRING, defaultValue = "")
@ReadOnlyProperty(name = "volume", dataType = DataType.INTEGER, defaultValue = "0")
public class Spotify extends Component {

    private static final Logger log = LoggerFactory.getLogger(Spotify.class);
    private SpotifyFacade spotifyFacade = new SpotifyFacade();

    public Spotify(Database database, Map<String, Object> options) {
        super(database, options);
        spotifyUpdateThread.start();
    }

    @Property(initialValue = "false")
    public void setPlaying(Boolean play) {
        if (play) {
            spotifyFacade.play();
        } else {
            spotifyFacade.pause();
        }
    }

    @Property(initialValue = "")
    public void setTrackURI(String trackURI) {
        spotifyFacade.play(trackURI);
    }

    private SpotifyUpdateThread spotifyUpdateThread = new SpotifyUpdateThread(spotifyFacade) {

        public void onUpdate(Status status) throws Exception {
            updatePropertyValue("playing", status.isPlaying());
            updatePropertyValue("volume", ((Double) status.getVolume()).intValue());

            if (status.getTrack() == null) {
                updatePropertyValue("length", 0);
                updatePropertyValue("trackURI", "");
                updatePropertyValue("artistURI", "");
                updatePropertyValue("albumURI", "");
                updatePropertyValue("track", "");
                updatePropertyValue("artist", "");
                updatePropertyValue("album", "");
            } else {
                updatePropertyValue("length", status.getTrack().getLength());
                updatePropertyValue("trackURI", status.getTrack().getTrackResource().getURI());
                updatePropertyValue("artistURI", status.getTrack().getArtistResource().getURI());
                updatePropertyValue("albumURI", status.getTrack().getAlbumResource().getURI());
                updatePropertyValue("track", status.getTrack().getTrackResource().getName());
                updatePropertyValue("artist", status.getTrack().getArtistResource().getName());
                updatePropertyValue("album", status.getTrack().getAlbumResource().getName());
            }
        }

    };

    @Override
    public void destroy() {
        spotifyUpdateThread.stop();
    }

}
