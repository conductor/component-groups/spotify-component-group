package com.github.soshibby.spotify.client;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Henrik on 18/11/2017.
 */
public class SpotifyAuthClient {

    private static final Logger log = LoggerFactory.getLogger(SpotifyAuthClient.class);
    private String url = "https://open.spotify.com";

    public HttpRequest get(String path) {
        log.info("Making GET request to {}.", url + path);

        return Unirest.get(url + path)
                .header("Referer", "https://open.spotify.com/embed?uri=spotify:user:spotify:playlist:3rgsDhGHZxZ9sB9DQWQfuf")
                .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
    }

}
