package com.github.soshibby.spotify.client;

import com.github.soshibby.spotify.utils.PortFinder;
import com.github.soshibby.spotify.exceptions.SpotifyConnectionTimeoutException;
import com.github.soshibby.spotify.exceptions.SpotifyResponseException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketTimeoutException;

/**
 * Created by Henrik on 17/11/2017.
 */
public class SpotifyLocalClient {

    private static final Logger log = LoggerFactory.getLogger(SpotifyLocalClient.class);
    private String url = "http://localhost";
    private String cfidToken = "";
    private String oauthToken = "";
    private int portNumber = 4375;
    private PortFinder portFinder = new PortFinder();
    private int connectionTimeout = 5000; // 5 seconds connection timeout.
    private int socketTimeout = 60000; // 60 seconds connection timeout.

    public SpotifyLocalClient() {
        Unirest.setTimeouts(connectionTimeout, socketTimeout);
    }

    public HttpRequest get(String path) {
        if (!portFinder.isValidPort(portNumber)) {
            portNumber = portFinder.findPort();
        }

        String url = buildUrl(path);
        log.info("Making GET request to {}", url);

        return Unirest.get(url)
                .queryString("ref", "")
                .queryString("cors", "")
                .queryString("_", "0")
                .queryString("oauth", oauthToken)
                .queryString("csrf", cfidToken)
                .header("Origin", "https://embed.spotify.com")
                .header("Referer", "https://embed.spotify.com/?uri=spotify:track:5Zp4SWOpbuOdnsxLqwgutt")
                .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36")
                .getHttpRequest();
    }

    public void send(HttpRequest request) {
        try {
            HttpResponse<String> response = request.asString();
            validateResponse(response);
        } catch (UnirestException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public <T> T send(HttpRequest request, Class<? extends T> responseClass) {
        HttpResponse<T> response;

        try {
            response = request.asObject(responseClass);
        } catch (UnirestException e) {
            if (e.getCause() instanceof SocketTimeoutException) {
                throw new SpotifyConnectionTimeoutException("Request to Spotify web helper did a timeout.", e);
            } else {
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        validateResponse(response);
        return response.getBody();
    }

    private String buildUrl(String path) {
        return url + ":" + portNumber + path;
    }

    public void setCFIDToken(String cfidToken) {
        this.cfidToken = cfidToken;
    }

    public void setOAuthToken(String oAuthToken) {
        this.oauthToken = oAuthToken;
    }

    private void validateResponse(HttpResponse response) {
        if (response.getStatus() >= 300) {
            log.error("Spotify responded with status {} with message: {}.", response.getStatus(), response.getBody());
            throw new SpotifyResponseException("Spotify responded with status " + response.getStatus() + " with message: " + response.getBody() + ".");
        }
    }

}
