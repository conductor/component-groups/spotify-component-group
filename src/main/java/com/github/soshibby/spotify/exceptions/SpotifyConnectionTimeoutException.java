package com.github.soshibby.spotify.exceptions;

/**
 * Created by Henrik on 18/11/2017.
 */
public class SpotifyConnectionTimeoutException extends RuntimeException {

    public SpotifyConnectionTimeoutException(String message) {
        super(message);
    }

    public SpotifyConnectionTimeoutException(String message, Exception innerException) {
        super(message, innerException);
    }

}
