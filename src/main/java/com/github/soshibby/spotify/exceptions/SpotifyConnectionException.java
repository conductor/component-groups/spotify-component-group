package com.github.soshibby.spotify.exceptions;

/**
 * Created by Henrik on 18/11/2017.
 */
public class SpotifyConnectionException extends RuntimeException{

    public SpotifyConnectionException(String message){
        super(message);
    }

}
