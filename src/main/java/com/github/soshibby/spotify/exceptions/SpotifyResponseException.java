package com.github.soshibby.spotify.exceptions;

/**
 * Created by Henrik on 18/11/2017.
 */
public class SpotifyResponseException extends RuntimeException {

    public SpotifyResponseException(String message) {
        super(message);
    }

}
