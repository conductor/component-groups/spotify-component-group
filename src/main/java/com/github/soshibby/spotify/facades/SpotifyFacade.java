package com.github.soshibby.spotify.facades;

import com.github.soshibby.spotify.mapper.UnirestMapper;
import com.github.soshibby.spotify.authentication.Authenticator;
import com.github.soshibby.spotify.client.SpotifyLocalClient;
import com.github.soshibby.spotify.response.Status;
import com.mashape.unirest.request.HttpRequest;

/**
 * Created by Henrik on 17/11/2017.
 */
public class SpotifyFacade {

    private SpotifyLocalClient localClient = new SpotifyLocalClient();
    private Authenticator authenticator;
    private int timeout = 60000; // 60 seconds timeout.

    public SpotifyFacade() {
        UnirestMapper.initMapper();
        authenticator = new Authenticator(localClient);
    }

    public void play() {
        authenticator.authenticate();

        HttpRequest request = localClient.get("/remote/pause.json")
                .queryString("pause", "false");

        localClient.send(request);
    }

    public void play(String trackURI) {
        authenticator.authenticate();

        HttpRequest request = localClient.get("/remote/play.json")
                .queryString("uri", trackURI);

        localClient.send(request);
    }

    public void pause() {
        authenticator.authenticate();

        HttpRequest request = localClient.get("/remote/pause.json")
                .queryString("pause", "true");

        localClient.send(request);
    }

    public Status getStatus() {
        authenticator.authenticate();

        HttpRequest request =  localClient.get("/remote/status.json")
                .queryString("returnon", "login,logout,play,pause,error,volume,ap")
                .queryString("returnafter", timeout);

        return localClient.send(request, Status.class);
    }

}
