package com.github.soshibby.spotify.authentication;

import com.github.soshibby.spotify.client.SpotifyAuthClient;
import com.github.soshibby.spotify.client.SpotifyLocalClient;
import com.github.soshibby.spotify.response.CFID;
import com.github.soshibby.spotify.response.SpotifyToken;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Henrik on 18/11/2017.
 */
public class Authenticator {

    private static final Logger log = LoggerFactory.getLogger(Authenticator.class);
    private SpotifyAuthClient authClient;
    private SpotifyLocalClient localClient;
    private DateTime lastOAuthRequest;
    private DateTime lastCFIDRequest;

    public Authenticator(SpotifyLocalClient localClient) {
       this.authClient = new SpotifyAuthClient();
       this.localClient = localClient;
    }

    public void authenticate() {
       if (hasCFIDKeyExpired()) {
           log.info("CFID key has expired, requesting a new key.");
           cfidAuthenticate();
       }

       if (hasOAuthTokenExpired()) {
           log.info("oAuth token has expired, requesting a new token");
           oauthAuthenticate();
       }
    }

    private void cfidAuthenticate() {
        CFID cfid = requestCFIDToken();
        localClient.setCFIDToken(cfid.getToken());
        lastCFIDRequest = DateTime.now();
    }

    private void oauthAuthenticate() {
        SpotifyToken token = requestOAuthToken();
        localClient.setOAuthToken(token.getToken());
        lastOAuthRequest = DateTime.now();
    }

    private CFID requestCFIDToken() {
        try {
            return localClient.get("/simplecsrf/token.json")
                    .asObject(CFID.class)
                    .getBody();
        } catch (UnirestException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private SpotifyToken requestOAuthToken() {
        try {
            return authClient.get("/token")
                    .asObject(SpotifyToken.class)
                    .getBody();
        } catch (UnirestException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private boolean hasCFIDKeyExpired() {
        if (lastCFIDRequest == null) {
            return true;
        }

        Period expires = new Period().withHours(2);
        return this.lastCFIDRequest.plus(expires).isBeforeNow();
    }

    private boolean hasOAuthTokenExpired() {
        if (lastOAuthRequest == null) {
            return true;
        }

        Period expires = new Period().withHours(2);
        return this.lastOAuthRequest.plus(expires).isBeforeNow();
    }

}
