package com.github.soshibby.spotify.utils;

import com.github.soshibby.spotify.exceptions.SpotifyConnectionException;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Henrik on 18/11/2017.
 */
public class PortFinder {

    private static final Logger log = LoggerFactory.getLogger(PortFinder.class);
    private int startPort = 4375;
    private int endPort = 4382;

    public int findPort() {
        int port = startPort;

        do {
            if (isValidPort(port)) {
                log.info("Found Spotify listening on port {}.", port);
                return port;
            }
            port++;
        } while (port <= endPort);

        throw new SpotifyConnectionException("Couldn\'t find the port that Spotify is listening on.");
    }

    public boolean isValidPort(int port) {
        try {
            Unirest.get("http://localhost:" + port).asString();
        } catch (UnirestException e) {
            log.debug("Spotify is not listening on port {}.", port);
            return false;
        }

        return true;
    }

}
