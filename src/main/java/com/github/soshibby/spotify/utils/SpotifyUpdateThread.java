package com.github.soshibby.spotify.utils;

import com.github.soshibby.spotify.exceptions.SpotifyConnectionException;
import com.github.soshibby.spotify.exceptions.SpotifyConnectionTimeoutException;
import com.github.soshibby.spotify.facades.SpotifyFacade;
import com.github.soshibby.spotify.response.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Henrik on 18/11/2017.
 */
public abstract class SpotifyUpdateThread implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(SpotifyUpdateThread.class);
    private SpotifyFacade spotifyFacade;
    private Thread thread;

    public SpotifyUpdateThread(SpotifyFacade spotifyFacade) {
        this.spotifyFacade = spotifyFacade;
    }

    public void start() {
        thread = new Thread(this);
        thread.setDaemon(true);
        thread.start();
    }

    public void stop() {
        if (thread != null) {
            thread.interrupt();
        }
    }

    public abstract void onUpdate(Status status) throws Exception;

    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    log.info("Fetching Spotify status.");
                    Status status = spotifyFacade.getStatus();
                    onUpdate(status);
                } catch (SpotifyConnectionTimeoutException e) {
                    log.error("Spotify connection timeout, this is expected to occur when nothing has changed in the Spotify client.");
                } catch (SpotifyConnectionException e) {
                    log.error("Unable to connect to local spotify web helper, is spotify running?");
                    Thread.sleep(1000);
                } catch (Exception e) {
                    log.error("Error when retreiving Spotify status, error was: " + e.getMessage(), e);
                    Thread.sleep(1000);
                }
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            log.warn("Spotify update thread was interrupted.", e);
        }

        log.error("Stopped receiving status messages.");
    }

}
