# Spotify Component Group For Conductor
Component group that is used for controlling Spotify with Conductor.

## Features
- Play, pause and change track
- Receive state changes e.g. current artist, track, album and track length

## Build Instructions
- Open a command prompt
- Run "gradle compile"
- You will now have a meta data file and a jar file in the "build/libs" folder.
- You can upload these files to the conductor repository "https://gitlab.com/conductor/repositories/conductor-repository".